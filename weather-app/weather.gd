extends Control

var check_time = 600.0
var time = 0.0
onready var http = HTTPRequest.new()
onready var label = get_node("Label")

func _ready():
	add_child(http)

	http.connect("request_completed", self, "_on_request_completed")

func _process(delta):
	time -= delta
	if time < 1:
		time = check_time
		check_weather()

func check_weather():
	http.request("http://wttr.in/?format=j1", [], true, HTTPClient.METHOD_GET)

func get_location(json):
	var data = json.result["nearest_area"][0]
	var area = data["areaName"][0]["value"]
	var country = data["country"][0]["value"]
	var region = data["region"][0]["value"]
	var location = area + " " + region + " " + country
	return location
	
func _on_request_completed(result, response_code, headers, body):
	if response_code == 200:
		var json = JSON.parse(body.get_string_from_utf8())
		var data = json.result["current_condition"][0]
		print(data)
		#most fonts don't seem to support ℉ so we use ° and F
		var temp_F = data["temp_F"] + "°F"
		var clouds = data["weatherDesc"][0]["value"]
		var location = get_location(json)
		label.text = temp_F + "\n" + clouds + "\n" + location
