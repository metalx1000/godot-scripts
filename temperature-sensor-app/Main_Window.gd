extends Control

const url = "https://filmsbykris.com/scripts/2021/templog.php"
var value = 25.0
var time_out = 0

onready var label = get_node("Label")
onready var circle = get_node("TextureProgress") 

onready var http_request = HTTPRequest.new()

func _ready():
	add_child(http_request)
	http_request.connect("request_completed", self, "_http_request_completed")
	
	#label.text = str(value) + "°F"
	http_req()
	
func _process(delta):
	time_out += delta
	if time_out > 30:
		time_out = 0
		http_req()
		
func update_charts(body):
	var i = 0
	for sensor in body.split("\n"):
		if sensor == "":
			return
			
		var stamp = sensor.split("|")[1]
		var temp = int(sensor.split("|")[2])
		var room = sensor.split("|")[4]
		

		var circle = get_tree().get_nodes_in_group("circles")[i]
		circle = circle.get_node("Control/TextureProgress")
		circle.value = int(temp)
		
		var r = range_lerp(temp, 40, 110, 0, 1)
		#var g = range_lerp(temp, 50, 80, 1, 0)
		var b = range_lerp(temp, 0, 70, 1, 0)
		circle.tint_progress = Color(r, 0, b)
		
		var label = circle.get_node("Label")
		label.text = str(temp) + "°F"
		
		var label2 = circle.get_node("Label2")
		label2.text = room
		
		var label3 = circle.get_node("Label3")
		label3.text = stamp
		
		i += 1

func http_req():
	var error = http_request.request(url)
	if error != OK:
		push_error("An error occurred in the HTTP request.")
		
func _http_request_completed(result, response_code, headers, body):
	update_charts(body.get_string_from_utf8())
