extends Control

var running = false
var start_time = 0

onready var button = get_node("VBoxContainer/Button")
onready var label = get_node("VBoxContainer/Label")

func _ready():
	load_time()
	
func _process(delta):
	if running:
		get_time()

func start():
	button.text = "STOP"
	running = true
	start_time = OS.get_unix_time()
	save_time(start_time)
	

func get_time():
	var time_now = OS.get_unix_time()
	var elapsed = time_now - start_time
	var seconds = elapsed % 60
	var hours = elapsed / 3600
	var minutes = (elapsed - (3600*hours)) / 60
	var elapsed_time = "%02d:%02d:%02d" % [hours ,minutes, seconds]
	label.text = elapsed_time
	
func stop():
	button.text = "START"
	running = false

func _on_Button_pressed():
	if !running:
		start()	
	else:
		stop()

func save_time(time):
	var config = ConfigFile.new()
	var err = config.load("user://settings.cfg")
	config.set_value("timer", "time", time)
	config.save("user://settings.cfg")

func load_time():
	var config = ConfigFile.new()
	var err = config.load("user://settings.cfg")
	start_time = config.get_value("timer","time",0)
	if start_time != 0:
		button.text = "STOP"
		running = true
