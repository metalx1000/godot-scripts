extends Node

var label
var input_box
var buttonbox
var buttons = []

var data =  [
	{"name":"John Smith","rank":"firefighter"},
	{"name":"Peter Piper","rank":"lieutenant"},
	{"name":"Jane Smith","rank":"firefighter"},
	{"name":"Jack Simpson","rank":"driver"},
	{"name":"Jack Jones","rank":"firefighter"},
	{"name":"Sally Jones","rank":"driver"},
	{"name":"Bob Packer","rank":"lieutenant"},
	{"name":"Jerry Johnson","rank":"firefighter"},
	{"name":"Chris Sanchez","rank":"firefighter"},
	{"name":"Andrew Poper","rank":"driver"},
	{"name":"Jennifer Jackson","rank":"firefighter"},
	{"name":"Jeff Apple","rank":"firefighter"},
	{"name":"Tim Samson","rank":"driver"},
	{"name":"Todd Sims","rank":"firefighter"},
	{"name":"Hal Cooler","rank":"lieutenant"},
	{"name":"Malcom Tedson","rank":"firefighter"},
	{"name":"Dan Danson","rank":"firefighter"},
	{"name":"Clair Danson","rank":"lieutenant"},
	{"name":"Jami Cline","rank":"firefighter"}
]
	

