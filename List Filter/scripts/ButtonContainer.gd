extends VBoxContainer
var button = preload("res://button.tscn")

func _ready():
	load_data()


func load_data():
	for person in Main.data:
		var btn = button.instance()
		btn.text = person.name
		btn.person = person.name
		btn.rank = person.rank
		add_child(btn)
		Main.buttons.append(btn)
