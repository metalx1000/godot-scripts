extends LineEdit


func _ready():
	Main.input_box = self

func _process(delta):
	grab_focus()


func _on_LineEdit_text_changed(new_text):
	#filter buttons based on text input
	for button in Main.buttons:
		if new_text.to_upper() in button.text.to_upper():
			button.visible = true
		else:
			button.visible = false 
		
		if new_text == "":
			button.visible = true


func _on_LineEdit_text_entered(new_text):
	for button in Main.buttons:
		if button.visible:
			Main.label.text = button.person +"\n"+button.rank
			break 

	text = ""
	_on_LineEdit_text_changed("")
