extends AnimatedSprite

var speed = rand_range(100,200)
var dead = false

onready var pop_snd = get_node("pop_snd")

func _ready():
	play("default")
	hue_shift()

func _process(delta):
	position.y -= speed * delta


func hue_shift():
	set_material(get_material().duplicate(true))
	# Offset sprite hue by a random value within specified limits.
	var rand_hue = float(randi() % 7)/2.0/3.2
	material.set_shader_param("Shift_Hue", rand_hue)

func pop():
	Global.score += 1
	pop_snd.play()
	play("death")
	yield(get_tree().create_timer(.5),"timeout")
	queue_free()

func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
	and event.button_index == BUTTON_LEFT \
	and event.is_pressed():
		if !dead:
			dead = true
			pop()


func _on_VisibilityNotifier2D_screen_exited():
	if !dead:
		Global.missed += 1
	#queue_free()
	position.y = get_viewport().size.y + 500

