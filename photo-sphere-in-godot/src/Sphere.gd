extends Spatial


onready var camera = get_node("Camera")
onready var sphere = get_node("Sphere")
onready var label = get_node("Label")

var last_x = 0
var last_y = 0

var delay = 0

func _ready():
	load_img("res://sphere_2.jpg")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	controls(delta)
	#check_sensor(delta)

func load_img(res):
	var material = SpatialMaterial.new()
	material.params_cull_mode = 2
	material.albedo_color = Color( 0.8, 0.8, 0.8, 1 )
	material.albedo_texture = load(res)
	material.metallic_specular = 0.0
	material.roughness = 0.0764706
	material.emission_enabled = true
	material.emission = Color( 0, 0, 0, 1 )
	material.emission_energy = 1.0
	material.emission_operator = 0
	material.emission_on_uv2 = false
	material.emission_texture = load(res)
	sphere.set_surface_material(0, material)
	
func controls(delta):
	keyboard_controls(delta)
	#touch()
	#mouse_controls(delta)
	
#func mouse_controls(delta):
func _input(event):
	if Input.is_action_just_pressed("mouse_click"):
		last_x = event.position.x
		last_y = event.position.y
		
	if Input.is_action_pressed("mouse_click"):
		if InputEventMouseMotion:
			var x = (last_x - event.position.x) * .005
			last_x = event.position.x
			var y = (last_y - event.position.y) * .005
			last_y = event.position.y
			update(x,y,1)
	
	touch(event)

func touch(event):
	
	if event is InputEventScreenDrag:
		if event.relative.x != 0:
			var x = -event.relative.x
			var y = -event.relative.y
			label.text = "Sensor: "
			label.text += "\n" + str(x)
			label.text += "\n" + str(y)
			update(x,y,.005)

func check_sensor(delta):
	var acc = Input.get_accelerometer()
	var mag = Input.get_magnetometer()
	var x = abs((mag.x * 5))
	var y = acc.z/5
		
	label.text = "Sensor: "
	label.text += "\n" + str(x)
	label.text += "\n" + str(y)
	camera.rotation.y = x * delta
	camera.rotation.x += y * delta
	camera.rotation.x = clamp(camera.rotation.x,-1.5,1.5)
	#update(x,y,delta)
		
func keyboard_controls(delta):
	var LEFT = Input.is_action_pressed("ui_left")
	var RIGHT = Input.is_action_pressed("ui_right")
	var UP = Input.is_action_pressed("ui_up")
	var DOWN = Input.is_action_pressed("ui_down")

	var x = int(LEFT) + -int(RIGHT)
	var y = int(UP) + -int(DOWN)
	update(x,y,delta)
	
func update(x,y,delta):
	camera.rotation.y += x * delta
	camera.rotation.x += y * delta
	camera.rotation.x = clamp(camera.rotation.x,-1.5,1.5)
